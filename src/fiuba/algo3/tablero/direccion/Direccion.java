package fiuba.algo3.tablero.direccion;

public abstract class Direccion {

	private int direccionX;
	private int direccionY;
	
	public Direccion(int valorX,int valorY){
		direccionX = valorX;
		direccionY = valorY;
	}

	public int getDireccionX(){
		return direccionX;
	}
	public int getDireccionY(){
		return direccionY;
	}
	public static Direccion obtenerDireccion(PuntoCardinal puntoCardinal){
		
		switch (puntoCardinal){
		case NORTE: return new Norte();
		case SUR: return new Sur();
		case ESTE: return new Este();
		case OESTE: return new Oeste();
		case NORESTE:return new Noreste();
		case NOROESTE: return new Noroeste();
		case SURESTE: return new Sureste();
		case SUROESTE: return new Suroeste();
		default: return null;

		}
	}
	
}
