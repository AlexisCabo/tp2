package fiuba.algo3.tablero;

public class Posicion {

	private int posicionX;
	private int posicionY;
	
	public Posicion(int valorX, int valorY) {
		if( (valorX < 0)  || (valorY < 0)) throw new PosicionInvalidaException();
		posicionX = valorX;
		posicionY = valorY;
	}

	public boolean equals( Object unObjeto ){	
		Posicion otraPosicion = (Posicion) unObjeto;
		return ( (this.posicionX == otraPosicion.posicionX) && (this.posicionY == otraPosicion.posicionY) );
	} 
	
	public int hashCode(){
		return ((posicionX*100)+posicionY);
	}

	public int getPosicionX() {
		return posicionX;
	}
	
	public int getPosicionY() {
		return posicionY;
	}
}
