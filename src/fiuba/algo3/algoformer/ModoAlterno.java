package fiuba.algo3.algoformer;

public class ModoAlterno extends Modo {

	public ModoAlterno(int ataque, int distanciaAtaque, int velocidad) {
		super(ataque, distanciaAtaque, velocidad);
	}

	@Override
	public String getModo() {
		return "modoAlterno";
	}

}
