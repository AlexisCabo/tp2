package fiuba.algo3.algoformer;

public class Bonecrusher extends Algoformer{

	public Bonecrusher() {
		super(200);
		modoHumanoide = new ModoHumanoide(30,3,1);
		modoAlterno = new ModoAlterno(30,3,8);
		modoActual = modoHumanoide;
	}

}
