package fiuba.algo3.algoformer;

public class Optimus extends Algoformer{

	
	public Optimus(){
		super(500);
		modoHumanoide = new ModoHumanoide(50,2,2);
		modoAlterno = new ModoAlterno(15,4,5);
		modoActual = modoHumanoide;
	}
}
