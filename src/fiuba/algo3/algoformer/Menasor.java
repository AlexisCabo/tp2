package fiuba.algo3.algoformer;

public class Menasor extends Algoformer{

	public Menasor(Optimus op,Bumblebee bum, Ratchet rat) {
		super(op.getVida() + bum.getVida() + rat.getVida());
		modoHumanoide = new ModoHumanoide(115,2,2);
		modoActual = modoHumanoide;
	}
	
	@Override
	public void transformar(){}

}
