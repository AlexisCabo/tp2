package fiuba.algo3.algoformer;

public class Frenzy extends Algoformer{

	public Frenzy() {
		super(400);
		modoHumanoide = new ModoHumanoide(10,5,2);
		modoAlterno = new ModoAlterno(25,2,6);
		modoActual = modoHumanoide;
	}

}
