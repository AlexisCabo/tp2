package fiuba.algo3.algoformer;

public class ModoHumanoide extends Modo{

	public ModoHumanoide(int ataque, int distanciaAtaque, int velocidad) {
		super(ataque, distanciaAtaque, velocidad);
	}

	@Override
	public String getModo() {
		return "modoHumanoide";
	}

}
