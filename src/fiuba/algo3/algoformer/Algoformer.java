package fiuba.algo3.algoformer;

import fiuba.algo3.interfaces.Agregable;
import fiuba.algo3.tablero.Casillero;
import fiuba.algo3.tablero.direccion.Direccion;

public abstract class Algoformer implements Agregable{

	private int vida;
	
	protected Modo modoHumanoide;
	protected Modo modoAlterno;
	protected Modo modoActual;
	
	private Casillero casillero;


	public Algoformer(int vida){
		this.vida = vida;
	}
	
	public int getVelocidad() {
		return modoActual.getVelocidad();
	}

	public void mover(Direccion direccion) {
		for (int i=0; i<getVelocidad(); i++){
			casillero.colocarEnSiguiente(this,direccion);
		}
	}

	public void ubicarEn(Casillero casillero) {
		this.casillero = casillero;
	}

	public Object getPosicion() {
		return casillero.getPosicion();
	}

	public void transformar() {
		if (modoActual == modoHumanoide) {
			modoActual=modoAlterno;
			return;
		}
		modoActual = modoHumanoide;
		
		
	}

	public String getModo() {
		return modoActual.getModo();
	}
	public int getVida(){
		return vida;
	}



}
