package fiuba.algo3.algoformer;

public class Bumblebee extends Algoformer{

	public Bumblebee() {
		super(350);
		modoHumanoide = new ModoHumanoide(40,1,2);
		modoAlterno = new ModoAlterno(20,3,5);
		modoActual = modoHumanoide;
	}

}
