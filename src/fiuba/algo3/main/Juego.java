package fiuba.algo3.main;

import fiuba.algo3.objetos.ChispaSuprema;
import fiuba.algo3.tablero.Tablero;
import fiuba.algo3.tablero.direccion.PuntoCardinal;

public class Juego {
	
	
	private final Tablero tablero;
	private final Jugador[] jugadores;
	private final ControladorDeEstados controladorEstados;
	
	
	public Juego(final int cantidadJugadores){
		if (cantidadJugadores >8) throw new CantidadJugadoresExedidaError();
		tablero = new Tablero();
		controladorEstados = new ControladorDeEstados();
		jugadores = new Jugador[cantidadJugadores];
		
	}


	public void inicializarJugadores() {
		for (int i = 0 ; i < jugadores.length; i++){
			jugadores[i]= new Jugador(i+1);
		}
		
	}

	
	//posiciona los jugadores y la chispa suprema en el tablero
	public void inicializarJuego() {
		for (Jugador jugador : jugadores){
			jugador.posicionarPersonajes(tablero);
		}
		
		tablero.agregarAPuntoCardinal(new ChispaSuprema(), PuntoCardinal.CENTRO,0);
	}
	
	
	

}
