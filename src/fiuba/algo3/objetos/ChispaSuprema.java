package fiuba.algo3.objetos;

import fiuba.algo3.interfaces.Agregable;
import fiuba.algo3.tablero.Casillero;

public class ChispaSuprema implements Agregable{
	private boolean suelta;
	
	public ChispaSuprema(){
		suelta = true;
	}

	@Override
	public void ubicarEn(Casillero casillero) {
		casillero.colocar(this);
	}
}
