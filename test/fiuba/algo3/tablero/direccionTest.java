package fiuba.algo3.tablero;

import org.junit.Assert;
import org.junit.Test;

import fiuba.algo3.tablero.direccion.*;

public class direccionTest {

	@Test
	public void testCreoDireccionNorteDevuelveEnX0EnY1(){
		Norte norte = new Norte();
		Assert.assertEquals(norte.getDireccionX(), 0);
		Assert.assertEquals(norte.getDireccionY(), 1);
	}
	
	@Test
	public void testCreoDireccionSurDevuelveEnX0EnYmenos1(){
		Sur sur = new Sur();
		Assert.assertEquals(sur.getDireccionX(), 0);
		Assert.assertEquals(sur.getDireccionY(), -1);
	}
	
	@Test
	public void testCreoDireccionEsteDevuelveEnX1EnY0(){
		Este este = new Este();
		Assert.assertEquals(este.getDireccionX(), 1);
		Assert.assertEquals(este.getDireccionY(), 0);
	}
	
	@Test
	public void testCreoDireccionOesteDevuelveEnXmenos1EnY0(){
		Oeste oeste = new Oeste();
		Assert.assertEquals(oeste.getDireccionX(),-1);
		Assert.assertEquals(oeste.getDireccionY(), 0);
	}

	@Test
	public void testCreoDireccionSuroesteDevuelveEnXmenos1EnYmenos1(){
		Suroeste suroeste = new Suroeste();
		Assert.assertEquals(suroeste.getDireccionX(), -1);
		Assert.assertEquals(suroeste.getDireccionY(), -1);
	}
	
	@Test
	public void testCreoDireccionSuresteDevuelveEnX1EnYmenos1(){
		Sureste sureste = new Sureste();
		Assert.assertEquals(sureste.getDireccionX(), 1);
		Assert.assertEquals(sureste.getDireccionY(), -1);
	}
	
	@Test
	public void testCreoDireccionNoroesteDevuelveEnXmenos1EnY1(){
		Noroeste noroeste = new Noroeste();
		Assert.assertEquals(noroeste.getDireccionX(), -1);
		Assert.assertEquals(noroeste.getDireccionY(), 1);
	}
	
	@Test
	public void testCreoDireccionNoresteDevuelveEnX1EnY1(){
		Noreste noreste = new Noreste();
		Assert.assertEquals(noreste.getDireccionX(), 1);
		Assert.assertEquals(noreste.getDireccionY(), 1);
	}
}
