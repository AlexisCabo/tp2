package fiuba.algo3.tablero;

import org.junit.Assert;
import org.junit.Test;

import fiuba.algo3.algoformer.Algoformer;
import fiuba.algo3.algoformer.Bumblebee;
import fiuba.algo3.algoformer.Optimus;
import fiuba.algo3.algoformer.Ratchet;

public class tableroTest {

	@Test
	public void testCreoTableroSeCreaConDimension20(){
		Tablero tablero = new Tablero();
		Assert.assertEquals(tablero.getDimension(), 20);
		
	
	}
	
	@Test
	public void testPosicionarPersonajes(){
		Algoformer[] algoformers= {new Optimus(),new Bumblebee(),new Ratchet()};
		Tablero tablero = new Tablero();
		int dimension = tablero.getDimension();

		for (int i = 0; i<dimension;i++){
			for (int j = 0; j<dimension; j++){
				Assert.assertTrue(tablero.estaLibreEn(new Posicion(i,j)));
			}
		}
		 tablero.inicializarAlgoformers(algoformers, 1); //posicionados en lado norte
		 Assert.assertFalse(tablero.estaLibreEn(new Posicion(dimension/2,0)));
		 Assert.assertFalse(tablero.estaLibreEn(new Posicion(dimension/2 +1,0)));
		 Assert.assertFalse(tablero.estaLibreEn(new Posicion(dimension/2 -1,0)));
		 Assert.assertTrue(tablero.estaLibreEn(new Posicion(dimension/2 +2,0)));
		 
		 tablero.inicializarAlgoformers(algoformers, 5); // posicionados en esquina noreste
		 Assert.assertFalse(tablero.estaLibreEn(new Posicion(dimension-1,0)));
		 Assert.assertFalse(tablero.estaLibreEn(new Posicion(dimension-1,1)));
		 Assert.assertFalse(tablero.estaLibreEn(new Posicion(dimension-2,0)));
		 Assert.assertTrue(tablero.estaLibreEn(new Posicion(dimension-1,2)));
	}
	
	
}
