package fiuba.algo3.integracion;

import org.junit.Assert;
import org.junit.Test;

import fiuba.algo3.algoformer.Algoformer;
import fiuba.algo3.algoformer.Optimus;
import fiuba.algo3.main.Juego;
import fiuba.algo3.tablero.Posicion;
import fiuba.algo3.tablero.Tablero;
import fiuba.algo3.tablero.direccion.Direccion;
import fiuba.algo3.tablero.direccion.Este;

public class integracionTest {

	
//	1. Se ubica un algoformer humanoide en un casillero, se pide que se mueva, se verifica
//	nueva posici�n acorde a su modo.
	@Test
	public void testSeUbicaUnAlgoformerYSeMueveVerificaPosicion(){	
		Algoformer algoformer = new Optimus();
		Tablero tablero = new Tablero();
		Posicion posicionInicial = new Posicion(2,3);
		Posicion posicionFinal = new Posicion( 2 + algoformer.getVelocidad(), 3);
		Direccion direccion = new Este();

		tablero.agregar(algoformer,posicionInicial);
		algoformer.mover(direccion);
		Assert.assertEquals(algoformer.getPosicion(), posicionFinal);
	}
	@Test
	public void testUbicaUnAlgoformerSeVerificaTransformacion(){
		Algoformer algoformer = new Optimus();
		Tablero tablero = new Tablero();
		Posicion posicion = new Posicion(2,3);
		tablero.agregar(algoformer, posicion);
		
		Assert.assertEquals(algoformer.getModo(),"modoHumanoide");
		
		algoformer.transformar();
		Assert.assertEquals(algoformer.getModo(),"modoAlterno");
		
		algoformer.transformar();
		Assert.assertEquals(algoformer.getModo(),"modoHumanoide");
	}
	@Test
	public void testUbicarAlgoformerModoAlternoVerificarVelocidad(){
		Algoformer algoformer = new Optimus();
		Tablero tablero = new Tablero();
		Posicion posicionInicial = new Posicion(2,3);
		algoformer.transformar();
		Posicion posicionFinal = new Posicion( 2 + algoformer.getVelocidad(),3);
		tablero.agregar(algoformer, posicionInicial);
		
		
		algoformer.mover(new Este());
		
		Assert.assertEquals(algoformer.getPosicion(), posicionFinal);
		
	}
	
	@Test
	public void testCrearJuegoInicializar(){
		int cantidadJugadores = 2;
		Juego juego = new Juego(cantidadJugadores);
		juego.inicializarJugadores();
		juego.inicializarJuego();
		
	}
	@Test
	public void testPrueba(){
		int corrimiento = -1;
		System.out.println((20-1 + corrimiento)>(20-1) ? (20-1) : (20-1 + corrimiento));
		System.out.println((0 + (-corrimiento))<(0) ? (0) : (0 + (-corrimiento)));
		/*(DIMENSION-1 + corrimiento)>(DIMENSION-1) ? DIMENSION-1 : 
			DIMENSION-1 + corrimiento,(0 + corrimiento) < 0 ? 0 : 0 + corrimiento)*/
	}
	

	
	
	
}
