package fiuba.algo3.algoformer;

import org.junit.Assert;
import org.junit.Test;

import fiuba.algo3.tablero.Casillero;
import fiuba.algo3.tablero.Posicion;
import fiuba.algo3.tablero.direccion.Direccion;
import fiuba.algo3.tablero.direccion.Este;

public class algoformerTest {
	
	@Test
	public void testTransformar(){
		Algoformer algoformer = new Bumblebee();
		
		Assert.assertTrue(algoformer.getModo() == "modoHumanoide");
		
		algoformer.transformar();
		Assert.assertTrue(algoformer.getModo() == "modoAlterno");
		
		algoformer.transformar();
		Assert.assertTrue(algoformer.getModo() == "modoHumanoide");
	//comentario2
	}
	

}
